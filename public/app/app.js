(function() {
    angular.module('todoApp', [
            'ui.bootstrap'
        ])
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$timeout'];

    function MainController($scope, $timeout) {

        var vm = this;
        vm.todo = undefined;

        var socket = io('https://lab5-assignments-cooljith91112.c9users.io:8081/');

        socket.emit('get my todos', {});
        socket.on('here is your todo', function(data) {
            vm.data = data;
            console.log(data);

            $timeout(function() {
                $scope.$apply();
            });


        });

        socket.on('here is your empty todo :P', function(data) {
            vm.data = null;

            $timeout(function() {
                $scope.$apply();
            });


        });


        vm.addTodo = function() {
            socket.emit('addTodo', {
                todoData: vm.todo,
                todoCompleted: false
            });
        };

        vm.todoChanged = function(id, newData) {

            socket.emit('todoUpdate', {
                id: id,
                todoCompleted: newData

            })
        }

        vm.deleteTodo = function(id) {
            socket.emit('todoDelete', {
                id: id
            })
        }


    }

})();