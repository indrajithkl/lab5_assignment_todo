  //c9 open ports :  8080, 8081, and 8082

  var express = require('express');
  var mongodb = require('mongodb');
  var app = express();
  var server = require('http').Server(app);
  var io = require('socket.io')(server);
  var port = 8081;


  var MongoClient = mongodb.MongoClient;

  var url = 'mongodb://localhost:27017/todo';
  var collectionName = 'todoCollection';

  var db;

  MongoClient.connect(url, function(err, dbs) {
    if (err) {
      console.log('Unable to connect to the mongoDB server. Error:', err);
    }
    else {
      //HURRAY!! We are connected. :)
      console.log('Connection established to', url);
      db = dbs;

    }
  });


  server.listen(port, function() {
    console.log('Server listening at port %d', port);
  });

  io.on('connection', function(socket) {
    // socket.emit('news', { hello: 'world' });
    // socket.on('my other event', function (data) {
    //   console.log(data);
    // });

    socket.on('get my todos', function(data) {

      getData();

    });

    socket.on('addTodo', function(data) {
      var collection = db.collection(collectionName);
      collection.insert({
        data: data.todoData,
        completed: data.todoCompleted

      });

      getData();

    });

    socket.on('todoUpdate', function(data) {
      var collection = db.collection(collectionName);
      var id = new mongodb.ObjectID(data.id);
      console.log(id);
      collection.update({
          '_id': id
        }, {
          $set: {
            'completed': data.todoCompleted
          }

        },
        function(err, results) {
          // console.log(err);
          console.log(data);
          // callback();
          getData();
        }
      );

    });

    socket.on('todoDelete', function(data) {
      var collection = db.collection(collectionName);
      var id = new mongodb.ObjectID(data.id);
      console.log(id);
      collection.remove({
        '_id': id
      }, function(err, results) {
        console.log(err)
        getData();
      });

    });


  });

  var getData = function() {
    // Get the documents collection
    var collection = db.collection(collectionName);
    collection.find().sort([
      ['_id', 'desc']
    ]).toArray(function(err, result) {
      if (err) {
        console.log(err);
      }
      else if (result.length) {
        // console.log('Found:', result);
        io.sockets.emit('here is your todo', result);
      }
      else {
        io.sockets.emit('here is your empty todo :P', result);
        console.log('No document(s) found with defined "find" criteria!');
      }
      //Close connection
      // db.close();
    });
  }



  // Routing
  app.use(express.static(__dirname + '/public'));